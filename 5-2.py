from math import sqrt
from math import log2

#служебные функции begin
def read():
    A = []
    fin = open('E:\python36\proj\TI_TCH_project\A.txt', 'r')
    s = fin.read()
    s = s[1:len(s) - 1]
    i = 0
    while i < len(s):
        word, i = get_word(s, i)
        i += 1
        if word[len(word) - 1] == ',':
            word = word[:len(word) - 1]
        A.append(int(word))
    return A

def get_word(s, pos):
    word = ''
    while pos < len(s) and s[pos] != ' ':
        word += s[pos]
        pos += 1
    return word, pos

def is_simple(num):
   for i in range(2, int(sqrt(num)) + 1):
       if num%i == 0:
           return False
   return True

 # класс для построения дерева кода хаффмана
class node:
    def __init__(self, left, right, var, num):
        self.left = left
        self.right = right
        self.var = var
        self.default_num = num


codes = []


# функция обхода дерева
def get_codes(s, nod):
    if nod.default_num != None:
        codes[nod.default_num] = s
    else:
        if nod.left:
            get_codes(s + '0', nod.left)
        if nod.right:
            get_codes(s + '1', nod.right)
#служебные функции end
n = 10000
file = open('result.txt', 'r')
arr = []
s = file.readline()
while len(s) > 1:
    a, b, c, d, e = s.split(',')
    arr.append((int(a), int(b), int(c), float(d), float(e)))
    s = file.readline()
file.close()
varrh = [0 for i in range(293)]
varrx = [0 for i in range(293)]
varrr = [0 for i in range(293)]
vmax = [0 for i in range(293)]
vmin = [9999 for i in range(293)]
for i in arr:
    varrh[i[0] - 1] += i[3]/10
    varrx[i[0] - 1] += i[4]/100000
    varrr[i[0] - 1] += 1
    if i[3] > vmax[i[0] - 1]:
        vmax[i[0] - 1] = i[3]
    if i[3] < vmin[i[0] - 1]:
        vmin[i[0] - 1] = i[3]
smid = ''
smax = ''
smin = ''
for i in range(293):
    if varrh[i] != 0:
        smid += str((varrh[i]/varrr[i])*10) + ','
        smax += str(vmax[i]) + ','
        smin += str(vmin[i]) + ','
    else:
        smid += str(0) + ','
        smax += str(0) + ','
        smin += str(0) + ','

fmid = open('smid.txt', 'w')
fmin = open('smin.txt', 'w')
fmax = open('smax.txt', 'w')
fmid.write(smid)
fmin.write(smin)
fmax.write(smax)
exit(0)