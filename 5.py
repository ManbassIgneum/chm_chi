from math import sqrt
from math import log2

#служебные функции begin
def read():
    A = []
    fin = open('E:\python36\proj\TI_TCH_project\A.txt', 'r')
    s = fin.read()
    s = s[1:len(s) - 1]
    i = 0
    while i < len(s):
        word, i = get_word(s, i)
        i += 1
        if word[len(word) - 1] == ',':
            word = word[:len(word) - 1]
        A.append(int(word))
    return A

def get_word(s, pos):
    word = ''
    while pos < len(s) and s[pos] != ' ':
        word += s[pos]
        pos += 1
    return word, pos

def is_simple(num):
   for i in range(2, int(sqrt(num)) + 1):
       if num%i == 0:
           return False
   return True

 # класс для построения дерева кода хаффмана
class node:
    def __init__(self, left, right, var, num):
        self.left = left
        self.right = right
        self.var = var
        self.default_num = num


codes = []


# функция обхода дерева
def get_codes(s, nod):
    if nod.default_num != None:
        codes[nod.default_num] = s
    else:
        if nod.left:
            get_codes(s + '0', nod.left)
        if nod.right:
            get_codes(s + '1', nod.right)
#служебные функции end
n = 10000
A = read()
s = ''
for p in range(100,300):
    if is_simple(p):
        parr = [0 for i in range(p)]
        for i in A:
            parr[i % p] += 1
        print(p)
        for a in range(p):
            for b in range(p):
                dots = []
                for el in range(p):
                    value = (pow(el, 3, p) + (el * a) % p + b) % p
                    eiler = pow(value, int((p - 1) / 2), p)
                    if eiler != (p - 1):
                        for i in range(int(p / 2) + 1):
                            if i * i % p == value:
                                if i == 0:
                                    dots.append((el, i))
                                else:
                                    dots.append((el, i))
                                    dots.append((el, p - i))
                # рассчитываем количество точек
                arr = [0 for i in dots]
                for x in range(p):
                    for j in range(len(dots)):
                        if dots[j][0] == x:
                            arr[j] += parr[x]

                num = 0
                for i in arr:
                    num += i

                # print(arr)
                tree = [node(None, None, arr[i], i) for i in range(len(arr)) if arr[i] != 0]

                # строим дерево
                while len(tree) > 1:
                    tree = sorted(tree, key=lambda j: j.var)
                    tree.append(node(tree[0], tree[1], tree[0].var + tree[1].var, None))
                    tree = tree[2:]

                codes = ['' for i in dots]
                get_codes('', tree[0])
                # print(codes)

                varr = arr
                # частоты
                for i in range(len(varr)):
                    varr[i] = varr[i] / num

                # print(arr)

                # энтропия шеннона
                sum = 0
                for i in varr:
                    if i != 0:
                        sum += (i * log2(i))

                sum *= -1
                # print(sum)

                # длина кода хаффмана
                len_h = 0
                for j in range(len(varr)):
                    len_h += varr[j] * len(codes[j])

                len_h *= 10000
                s += str(p) + ',' + str(a) + ',' + str(b) + ',' + str(sum) + ',' + str(len_h)  + '\n'
                # print(len_h)
file = open('result.txt', 'w')
file.write(s)
file.close()
exit(0)