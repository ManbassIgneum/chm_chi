from math import sqrt

#служебные функции begin
def read():
    A = []
    fin = open('E:\python36\proj\TI_TCH_project\A.txt', 'r')
    s = fin.read()
    s = s[1:len(s) - 1]
    i = 0
    while i < len(s):
        word, i = get_word(s, i)
        i += 1
        if word[len(word) - 1] == ',':
            word = word[:len(word) - 1]
        A.append(int(word))
    return A

def get_word(s, pos):
    word = ''
    while pos < len(s) and s[pos] != ' ':
        word += s[pos]
        pos += 1
    return word, pos

def is_simple(num):
   for i in range(2, int(sqrt(num)) + 1):
       if num%i == 0:
           return False
   return True
# служебные функции end


def div(a, b):
    for i in range(103):
        if (b * i)%103 == a:
            return i

# операция в группе точек эллиптической кривой
def plus(a,b):
    x = 0
    y = 0
    if a == (-1, -1):
        return b
    if b == (-1, -1):
        return a
    if a[0] != b[0]:
        x = pow(div((b[1] - a[1])%103,(b[0] - a[0])%103),2, 103) - a[0] - b[0]
        y = div((b[1] - a[1])%103,(b[0] - a[0])%103)*(a[0] - x) - a[1]
    elif a[0] == b[0] and a[1] == (103 - b[1])%103:
        return(-1, -1)
    elif a[0] == b[0] and a[1] == b[1]:
        alpha = div(3*a[0]*a[0]%103, 2*a[1]%103)
        x = alpha*alpha - 2*a[0]
        y = alpha*(a[0] - x) - a[1]
    else:
        print('mistakes were made')
    return (x % 103, y % 103)

n = 10000
sum = 0
arr = [i for i in range(103)]
a = 0
b = 95
p = 103
els = []
resarr = [(-1, -1)]
for el in range(len(arr)):
    value = (pow(el, 3, p) + (el * a) % p + b) % p
    eiler = pow(value, int((p - 1) / 2), p)
    if eiler != (p - 1):
        for i in range(int(p/2) + 1):
            if i*i % p == value:
                if i == 0:
                    resarr.append((el, i))
                else:
                    resarr.append((el, i))
                    resarr.append((el, p - i))
print('множество точек: ' + str(resarr))
poryadok = [0 for i in resarr]
elements = []
print(len(resarr))
pos = 0
for i in resarr:
    cur = i
    print('\nfor ' + str(cur) + ': ')
    elements.append([])
    while cur != (-1, -1):
        elements[pos].append(cur)
        print(str(cur) + ' ', end = ' '),
        poryadok[pos] += 1
        cur = plus(cur ,i)
    elements[pos].append(cur)
    print(str(cur) + ' ', end=' '),
    poryadok[pos] += 1
    pos += 1

print('\n\nпорядки:' + str(poryadok))
checker = []
for i in elements[14]:
    for j in elements[6]:
        checker.append(plus(i, j))
checker = sorted(checker)
print('группа, полученная прямой суммой 15 и 7 подгруппы ((9,0) и (5,23)): ' + str(checker))
print('порядок полученной группы: ' + str(len(checker)))
exit(0)