from math import sqrt
from math import log2

#служебные функции begin
def read():
    A = []
    fin = open('E:\python36\proj\TI_TCH_project\A.txt', 'r')
    s = fin.read()
    s = s[1:len(s) - 1]
    i = 0
    while i < len(s):
        word, i = get_word(s, i)
        i += 1
        if word[len(word) - 1] == ',':
            word = word[:len(word) - 1]
        A.append(int(word))
    return A

def get_word(s, pos):
    word = ''
    while pos < len(s) and s[pos] != ' ':
        word += s[pos]
        pos += 1
    return word, pos

def is_simple(num):
   for i in range(2, int(sqrt(num)) + 1):
       if num%i == 0:
           return False
   return True

#служебные функции end

minval = 8744
a = 0
b = 95
p = 103
n = 10000
A = read()
arr = [0 for i in range(p)]
res = [[] for i in range(p)]
dots = []
for x in range(p):
    value = (pow(x, 3, p) + (x * a) % p + b) % p
    eiler = pow(value, int((p - 1) / 2), p)
    if eiler != (p - 1):
        for i in range(int(p / 2) + 1):
            if i * i % p == value:
                if i == 0:
                    dots.append((x, i))
                else:
                    dots.append((x, i))
                    dots.append((x, p - i))
arrr = [0 for i in dots]
for i in A:
    arr[i%p] += 1
    for j in range(len(dots)):
        if dots[j][0] == i%p:
            arrr[j] += 1
# рассчитываем количество точек

num = 0
for i in arrr:
    num += i

print(arrr)

# класс для построения дерева кода хаффмана
class node:
    def __init__(self, left, right, var, num):
        self.left = left
        self.right = right
        self.var = var
        self.default_num = num

codes = ['' for i in range(len(arrr))]

# функция обхода дерева
def get_codes(s, nod):
    if nod.default_num != None:
        codes[nod.default_num] = s
    else:
        if nod.left:
            get_codes(s + '0', nod.left)
        if nod.right:
            get_codes(s + '1', nod.right)

tree = [node( None, None, arrr[i], i) for i in range(len(arrr))]

# строим дерево
while len(tree) > 1:
    tree = sorted(tree, key=lambda j:j.var)
    tree.append(node(tree[0], tree[1], tree[0].var + tree[1].var, None))
    tree = tree[2:]

get_codes('', tree[0])
print(codes)

# частоты
for i in range(len(arrr)):
    arrr[i] = arrr[i] / num

print(arrr)

# энтропия шеннона
sum = 0
for i in arrr:
    if i != 0:
        sum += (i * log2(i))

sum *= -1
print(sum)

# длина кода хаффмана
len_h = 0
for j in range(len(arrr)):
    len_h += arrr[j]*len(codes[j])

len_h *= 10000
print(len_h)

exit(0)