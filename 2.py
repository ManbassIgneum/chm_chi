from math import sqrt

#служебные функции begin
def read():
    A = []
    fin = open('E:\python36\proj\TI_TCH_project\A.txt', 'r')
    s = fin.read()
    s = s[1:len(s) - 1]
    i = 0
    while i < len(s):
        word, i = get_word(s, i)
        i += 1
        if word[len(word) - 1] == ',':
            word = word[:len(word) - 1]
        A.append(int(word))
    return A

def get_word(s, pos):
    word = ''
    while pos < len(s) and s[pos] != ' ':
        word += s[pos]
        pos += 1
    return word, pos

def is_simple(num):
   for i in range(2, int(sqrt(num)) + 1):
       if num%i == 0:
           return False
   return True
# служебные функции end

n = 10000
maxsum = 0
maxa = 0
maxb = 0
maxp = 0
A = read()
for p in range(100,300):
   if is_simple(p):
       arr = [0 for i in range(p)]
       # рассчитываем x mod p
       for i in A:
           arr[i%p] += 1
       # перебираем a, b in range(p)
       for b in range(p):
           for a in range(p):
               sum = 0
               for el in range(len(arr)):
                   value = (pow(el,3,p) + (el*a)%p + b)%p
                   # считаем критерий эйлера
                   eiler = pow(value, int((p - 1)/2), p)
                   if eiler != (p - 1):
                       sum += arr[el]
               if sum > maxsum:
                   maxsum = sum
                   maxa = a
                   maxb = b
                   maxp = p


print(maxsum)
print('a = ' + str(maxa))
print('b = ' + str(maxb))
print('p = ' + str(maxp))
exit(0)

exit(0)