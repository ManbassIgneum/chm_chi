from math import sqrt
from math import log2

#служебные функции begin
def read():
    A = []
    fin = open('E:\python36\proj\TI_TCH_project\A.txt', 'r')
    s = fin.read()
    s = s[1:len(s) - 1]
    i = 0
    while i < len(s):
        word, i = get_word(s, i)
        i += 1
        if word[len(word) - 1] == ',':
            word = word[:len(word) - 1]
        A.append(int(word))
    return A

def get_word(s, pos):
    word = ''
    while pos < len(s) and s[pos] != ' ':
        word += s[pos]
        pos += 1
    return word, pos

def is_simple(num):
   for i in range(2, int(sqrt(num)) + 1):
       if num%i == 0:
           return False
   return True
# служебные функции end

n = 10000
minsum = 1e9
A = read()
for p in range(100,300):
   if is_simple(p):
       P = [0 for i in range(p)]
       sum = 0.0
       for i in A:
           val = i%p
           P[val] += 1
       for i in P:
           sum += (i/n - 1/p)**2
       if sum < minsum:
           minsum = sum
           ans = p
           
print(ans)
print(minsum)

exit(0)